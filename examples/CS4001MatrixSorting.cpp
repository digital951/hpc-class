// run with g++ -std=c++0x filename.cpp

#include <iostream>
#include <vector>
#include <random>
#include <cstdlib>
#include <chrono>       // timing library

// Uncomment one of these to run one of the below algorithms
// #define SLOW
// #define MEDIUM
// #define FAST
// #define FASTER

#ifdef SLOW             // 2-D arrays on the heap
void sortColumns(std::vector<std::vector<int> >& array, int size);
#else                   // everyone else, usually 1-D
void sortColumns(std::vector<int>& array, int size);
#endif

int main(int argc, char *argv[]) {
    const int size = atoi(argv[1]);
    
    std::random_device rd;  // random library, replaces C's rand() function.
    std::default_random_engine gen(rd());
    std::uniform_int_distribution<int> d(0,9); // generate values from [0,9]

#ifdef SLOW
    std::vector<std::vector<int> > array(size, std::vector<int> (size));  // generate a 2-D vector
    for (int i=0;i<size;++i)
        for (int j=0;j<size;++j)
            array[i][j] = d(gen);  // fill with random values [0,9]
#else
    std::vector<int> array(size * size);  // generate 1-D array and fill with random values.
    for (int i=0;i<size*size;++i)
        array[i] = d(gen);
#endif

    // time the function used to sort the columns
    std::chrono::high_resolution_clock::time_point start = std::chrono::high_resolution_clock::now();
    sortColumns(array, size);
    std::chrono::high_resolution_clock::time_point end = std::chrono::high_resolution_clock::now();

    std::cout << "Time to sort the columns: " << (std::chrono::duration_cast<std::chrono::duration<double> >(end - start)).count() << " seconds.\n";

    return 0;
}

#if defined(SLOW)
// standard quicksort without randomly generated pivot point
void sort (std::vector<std::vector<int> >& array, int start, int stop, int size, int pos) {
    if (start >= stop) return;
    
    int mid = (start + stop) / 2;
    int i = start, j = stop, pivot = array[mid][pos]; // get pivot point, middle position of each column
    
    while (true) {
        while (array[i][pos] < pivot) i++;    // work your way in towards the pivot point
        while (array[j][pos] > pivot) j--;
        if (i > j) break;
        std::swap(array[i++][pos], array[j--][pos]);  // swap
    }
    
    sort (array, start, j, size, pos);  // recursively call per each time
    sort (array, i, stop, size, pos);
}

void sortColumns(std::vector<std::vector<int> >& array, int size) {
    // loop through each column and call quicksort on each column.
    for (int i=0;i<size;++i)
        sort(array, 0, size-1, size, i);
}

#elif defined(MEDIUM)
// similar to above sorting algorith, difference is using arr[i * colWidth + j] for arr[i][j]
void sort (int *array, int start, int stop, int size, int pos) {
    if (start >= stop) return;
    
    int mid = (start + stop) / 2;
    int i = start, j = stop, pivot = array[mid * size + pos];
    
    while (true) {
        while (array[i * size + pos] < pivot) i++;
        while (array[j * size + pos] > pivot) j--;
        if (i > j) break;
        std::swap(array[i++ * size + pos], array[j-- * size + pos]);
    }
    
    sort (array, start, j, size, pos);
    sort (array, i, stop, size, pos);
}

void sortColumns(std::vector<int>& array, int size) {
    // same as above
    for (int i=0;i<size;++i)
        sort(&array[0], 0, size-1, size, i);
}

#elif defined(FAST)
// same as std::sort call, nothing special here.
void sort (int *array, int start, int stop) {
    if (start >= stop) return;
    
    int mid = (start + stop) / 2;
    int i = start, j = stop, pivot = array[mid];
    
    while (true) {
        while (array[i] < pivot) i++;
        while (array[j] > pivot) j--;
        if (i > j) break;
        std::swap(array[i++], array[j--]);
    }
    
    sort (array, start, j);
    sort (array, i, stop);
}

void sortColumns(std::vector<int>& array, int size) {
    // temp array to store each column in.
	std::vector<int> output(size);
    
	for (int i=0;i<size;++i) {
        for (int j=0;j<size;++j)
            output[j] = array[j*size+i]; // store current column into output.
        
    	sort(&output[0], 0, size-1);  // sort the output array
        
        for (int j=0;j<size;++j)
            array[j*size+i] = output[j]; // store back into the original matrix
    }
}

#else
/*
    similar to below code but with loop tiling/blocking:
    for (int i=0;i<size-1;++i)
        for (int j=i+1;j<size;++J)
            std::swap(array[i*size+j], array[j*size+i]);
*/
void transpose(int *array, int *output, int size) {
    const int line = 64 / sizeof(int);  // number of int's allowed to fit in each cacheline
                                        // on babbage's cpu
    
    for (int ii=0;ii<size;ii+=line) {  // jumps by line distance each pass
        const int stopi = std::min(ii+line,size); // if at the end just go to size otherwise curr+line
        for (int jj=0;jj<size;jj+=line) {   // similar
            const int stopj = std::min(jj+line,size);
            for (int i=ii;i<stopi;i++)     // loop through line times and do everything in block form
                for (int j=jj;j<stopj;j++) // instead of all at once like above code.
                    output[i*size+j] = array[j*size+i];
        }
    }
}

// same as above sorting algorithm
void sort (int *array, int start, int stop) {
    if (start >= stop) return;
    
    int mid = (start + stop) / 2;
    int i = start, j = stop, pivot = array[mid];
    
    while (true) {
        while (array[i] < pivot) i++;
        while (array[j] > pivot) j--;
        if (i > j) break;
        std::swap(array[i++], array[j--]);
    }
    
    sort (array, start, j);
    sort (array, i, stop);
}

void sortColumns(std::vector<int>& array, int size) {
	std::vector<int> output(size*size); // temp array sized NxN
    
	transpose(&array[0], &output[0], size); // transpose the original matrix
	
    for (int i=0;i<size;++i)
    	sort(&output[i*size], 0, size-1);  // sort each row one
    
	transpose(&output[0], &array[0], size); // transpose back into original array
}
#endif
