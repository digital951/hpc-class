var skipLinks = {};

/**
 * This will focus on the first visible link in the content frame.
 */
skipLinks.focusOnContentFrame = function( contentFrameId, secondaryContentFrameId )
{
  var contentFrame = skipLinks.getExtendedIframe( contentFrameId );
  // if the frame exists and has frames itself, try the secondary frame id
  if ( contentFrame && contentFrame.frames && contentFrame.frames.length > 0 )
  {
    if ( contentFrame.frames[ secondaryContentFrameId ] )
    {
      contentFrame = contentFrame.frames[ secondaryContentFrameId ]
    }
  }
  
  // if the frame exists and has access to Prototype $$ utility
  if ( contentFrame && contentFrame.$$ )
  {
    
    var focusElement, primarySkipLink, firstHeader1, firstLink;
    
    // try to focus on an element that's  explicitly marked as the primary skiplink
    if ( (primarySkipLink = contentFrame.$$( '.primarySkipLink' )).length > 0 ) 
    {
      focusElement = primarySkipLink.first();
    }
    
    // try to focus on the first h1
    else if ( (firstHeader1 = contentFrame.$$( 'body h1:first' )).length > 0 )
    {
      focusElement = firstHeader1.first();
    }

    // next try to focus on the first non-hidden link
    else if ( (firstLink = contentFrame.$$( 'body a[class!="hideoff"]' )).length > 0 )
    {
      focusElement = firstLink.first();
    }
    
    if ( focusElement ) 
    {
      focusElement.focus();
      return false;
    }
    
  }
  
  // if the content frame is there but no Prototype, then just focus it
  else if ( contentFrame )
  {
    contentFrame.focus();
  }

  return false;
};

/**
 * This will focus on the first tab link in the top navigation bar, e.g. My Institution.
 */
skipLinks.focusOnSystemNav = function()
{
  var extIframe = skipLinks.getExtendedIframe( 'navFrame' );
  var tabs = extIframe.$$( '#appTabList td' );
  // focus on the first tab link.  the index is 1 because "Skip To: Main Content" will always be 0
  var firstTab = tabs[ 1 ];
  $( firstTab ).down('a').focus();
  return false;
};

/**
 * This will focus on the object with the passed in focusId.
 */
skipLinks.focusOn = function( focusId )
{
  var focusObj = $$( focusId );
  if ( focusObj && focusObj.length > 0 )
  {
    $( focusObj[ 0 ] ).focus();
  }
  return false;
};

/**
 * This is used to get a Prototype extended frame while being in another frame.
 * An example is calling into the content frame from the top navigation.
 */
skipLinks.getExtendedIframe = function( iframeName )
{
  // try to get the iframe from the top frames
  var iframe = window.top.frames[ iframeName ];
  if ( !iframe )
  {
    // this could be a sub-frame of contentFrame
    iframe = window.parent.frames[ iframeName ];
  }
  // try again to get the iframe.  this will be the first frame for nav and
  // the second frame for content.  this is a workaround for Firefox.
  if ( !iframe && window.top.frames.length == 2 )
  {
    iframe = iframeName == 'navFrame' ? window.top.frames[ 0 ] : window.top.frames[ 1 ];
  }
  // if we don't have a frame to work with, just return null
  if ( !iframe )
  {
    return null;
  }

  var idoc;
  var iwin;
  try
  {
    // this will throw an exception if the iframe is an external URL
    var iframeLocation = iframe.location;

    // get window and document objects for iframe (IE compatible)
    if ( iframe.contentDocument || ( iframe.contentWindow && iframe.contentWindow.document ) )
    {
      idoc = iframe.contentDocument || iframe.contentWindow.document;
    }
    if ( iframe.contentWindow || ( iframe.contentDocument && iframe.contentDocument.defaultView ) )
    {
      iwin = iframe.contentWindow || iframe.contentDocument.defaultView;
    }
  }
  catch( err )
  {
    // if we are in here, then most likely we tried to access an
    // external site, which is not allowed.  just return the iframe.
    iframe.focus();
    return null;
  }

  // extend prototype if available and return
  if ( iwin && iwin.Prototype )
  {
    Element.extend( idoc );
    iwin.Element.extend( idoc );
    return iwin;
  }
  else if ( iframe && iframe.Prototype )
  {
    return iframe;
  }

  return null;
};
