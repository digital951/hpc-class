var frameset = {};

frameset.getTopFrameHeight = function()
{
  return $('navFrame').getHeight();
};

frameset.setTopFrameHeight = function( height )
{
  $('navFrame').setStyle({height: height + 'px'});
  frameset.onResize();
};

frameset.onResize = function(ev)
{
  var windowHeight = document.viewport.getHeight();
  var topFrameHeight = $('navFrame').getHeight();
  var contentFrame = $('contentFrame');
  contentFrame.hide();
  contentFrame.setStyle({height: (windowHeight - topFrameHeight) + 'px'});
  contentFrame.show();
};

frameset.openHelpWindow = function( helpUrl )
{
  var features='width=900, height=675, toolbar=yes, location=yes, menubar=yes, scrollbars=yes, status=yes, resizable=yes';
  newWindow=window.open(helpUrl,'_blank',features);
  if(newWindow != null){
    newWindow.focus();
  }

  return false;
};

